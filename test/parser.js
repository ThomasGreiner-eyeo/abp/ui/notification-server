/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {deepEqual, strict} = require("assert");
const {equal} = strict;

const {loadNotifications} = require("../lib/parser");

const HOUR_IN_MS = 60 * 60 * 1000;

function formatTime(timestamp)
{
  const date = new Date(timestamp);
  return date.toISOString().substring(0, 16);
}

function getFileWithTimeRange(startOffset, endOffset)
{
  const currentTime = Date.now();
  const startTime = currentTime + HOUR_IN_MS * startOffset;
  const endTime = currentTime + HOUR_IN_MS * endOffset;

  return [
    ["1",
`start = ${formatTime(startTime)}
end = ${formatTime(endTime)}`
    ]
  ];
}

describe("Notification parser", () =>
{
  it("typical", () =>
  {
    const files = [
      ["1",
`severity = information
title.en-US = The title
message.en-US = The message`
      ]
    ];

    const notifs = loadNotifications(files);
    deepEqual(notifs, [
      {
        id: "1",
        severity: "information",
        title: {
          "en-US": "The title"
        },
        message: {
          "en-US": "The message"
        }
      }
    ]);
  });

  it("inactive", () =>
  {
    const files = [
      ["1", "\ninactive = Yes\n"],
      ["2", "\ninactive = No\n"]
    ];

    const notifs = loadNotifications(files);
    equal(notifs.length, 2);
    equal(notifs[0].inactive, true);
    equal(notifs[1].inactive, false);
  });

  it("in range", () =>
  {
    const files = getFileWithTimeRange(-1, 1);

    const notifs = loadNotifications(files);
    equal(notifs.length, 1);
    equal(notifs[0].id, "1");
    equal("inactive" in notifs[0], false);
  });

  it("after range", () =>
  {
    const files = getFileWithTimeRange(-2, -1);

    const notifs = loadNotifications(files);
    equal(notifs.length, 1);
    equal(notifs[0].inactive, true);
  });

  it("before range", () =>
  {
    const files = getFileWithTimeRange(1, 2);

    const notifs = loadNotifications(files);
    equal(notifs.length, 1);
    equal(notifs[0].inactive, true);
  });

  it("start and end not present", () =>
  {
    const files = getFileWithTimeRange(-1, 1);

    const notifs = loadNotifications(files);
    equal(notifs.length, 1);
    equal("inactive" in notifs[0], false);
    equal("start" in notifs[0], false);
    equal("end" in notifs[0], false);
  });

  it("interval", () =>
  {
    const files = [
      ["1", "\ninterval = 100\n"],
      ["2", "\ninterval = onehundred\n"]
    ];

    const notifs = loadNotifications(files);
    equal(notifs.length, 1);
    equal(notifs[0].interval, 100);
  });

  it("severity", () =>
  {
    const files = [
      ["1", "\nseverity = information\n"],
      ["2", "\nseverity = critical\n"],
      ["3", "\nseverity = normal\n"],
      ["4", "\nseverity = relentless\n"]
    ];

    const notifs = loadNotifications(files);
    equal(notifs.length, 4);
    equal(notifs[0].severity, "information");
    equal(notifs[1].severity, "critical");
    equal(notifs[2].severity, "normal");
    equal(notifs[3].severity, "relentless");
  });

  it("urls", () =>
  {
    const files = [
      ["1", "\nurls = adblockplus.org\n"],
      ["1", "\nurls = adblockplus.org eyeo.com\n"]
    ];

    const notifs = loadNotifications(files);
    equal(notifs.length, 2);
    deepEqual(notifs[0].urlFilters, ["ADBLOCKPLUS.ORG^$document"]);
    deepEqual(notifs[1].urlFilters, [
      "ADBLOCKPLUS.ORG^$document",
      "EYEO.COM^$document"
    ]);
  });

  it("target", () =>
  {
    const files = [
      ["1", "\ntarget = extension=adblockplus\n"],
      ["2", "\ntarget = extensionVersion=1.2.3\n"],
      ["3", "\ntarget = extensionVersion>=1.2.3\n"],
      ["4", "\ntarget = extensionVersion<=1.2.3\n"],
      ["5", "\ntarget = application=chrome\n"],
      ["6", "\ntarget = applicationVersion=1.2.3\n"],
      ["7", "\ntarget = applicationVersion>=1.2.3\n"],
      ["8", "\ntarget = applicationVersion<=1.2.3\n"],
      ["9", "\ntarget = platform=chromium\n"],
      ["10", "\ntarget = platformVersion=1.2.3\n"],
      ["11", "\ntarget = platformVersion>=1.2.3\n"],
      ["12", "\ntarget = platformVersion<=1.2.3\n"],
      ["13", "\ntarget = blockedTotal=10\n"],
      ["14", "\ntarget = blockedTotal>=10\n"],
      ["15", "\ntarget = blockedTotal<=10\n"],
      ["16", "\ntarget = locales=en-US\n"],
      ["17", "\ntarget = locales=en-US,de-DE\n"]
    ];

    const notifs = loadNotifications(files);
    equal(notifs.length, 17);
    deepEqual(notifs[0].targets, [{extension: "adblockplus"}]);
    deepEqual(notifs[1].targets, [{
      extensionMinVersion: "1.2.3",
      extensionMaxVersion: "1.2.3"
    }]);
    deepEqual(notifs[2].targets, [{extensionMinVersion: "1.2.3"}]);
    deepEqual(notifs[3].targets, [{extensionMaxVersion: "1.2.3"}]);
    deepEqual(notifs[4].targets, [{application: "chrome"}]);
    deepEqual(notifs[5].targets, [{
      applicationMinVersion: "1.2.3",
      applicationMaxVersion: "1.2.3"
    }]);
    deepEqual(notifs[6].targets, [{applicationMinVersion: "1.2.3"}]);
    deepEqual(notifs[7].targets, [{applicationMaxVersion: "1.2.3"}]);
    deepEqual(notifs[8].targets, [{platform: "chromium"}]);
    deepEqual(notifs[9].targets, [{
      platformMinVersion: "1.2.3",
      platformMaxVersion: "1.2.3"
    }]);
    deepEqual(notifs[10].targets, [{platformMinVersion: "1.2.3"}]);
    deepEqual(notifs[11].targets, [{platformMaxVersion: "1.2.3"}]);
    deepEqual(notifs[12].targets, [{
      blockedTotalMin: 10,
      blockedTotalMax: 10
    }]);
    deepEqual(notifs[13].targets, [{blockedTotalMin: 10}]);
    deepEqual(notifs[14].targets, [{blockedTotalMax: 10}]);
    deepEqual(notifs[15].targets, [{locales: ["en-US"]}]);
    deepEqual(notifs[16].targets, [{locales: ["en-US", "de-DE"]}]);
  });
});
