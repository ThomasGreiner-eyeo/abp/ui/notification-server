# Notification server

Backend for Adblock Plus notifications. It provides the following URLs:

- */notification.json* - Return notifications to show

See [notification specification][spec] for more details.

[spec]: https://gitlab.com/eyeo/specs/spec/-/blob/master/spec/abp/notifications.md
