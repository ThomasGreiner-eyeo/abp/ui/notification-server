/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

const fs = require("fs");

const {loadNotifications} = require("../lib/parser");

// TODO: NYI
const outputPath = "";

function generateNotifications()
{
  // TODO: NYI
  const files = [];

  // Ignoring notifications with variants here - we can only process those in a
  // URL handler
  const notifs = loadNotifications(files)
    .filter((notif) => "variants" in notif);

 const output = {
   notifications: notifs
   // 'version': time.strftime('%Y%m%d%H%M', time.gmtime()),
 };

 fs.writeFile(outputPath, JSON.stringify(output));
}

if (require.main === module)
{
  generateNotifications();
}
