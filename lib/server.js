/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const http = require("http");
const qs = require("querystring");
const url = require("url");

const {loadNotifications} = require("./parser");

// TODO: for testing only
let FILES = [];

function setFiles(files)
{
  FILES = files;
}
exports.setFiles = setFiles;

function determineGroups(version, notifications)
{
  const versionGroups = new Map();
  version.split("-")
    .slice(1)
    .filter((x) => x.includes("/"))
    .forEach((x) =>
    {
      const [id, variant] = x.split("/", 2);
      versionGroups.set(id, variant);
    });

  const groups = [];

  for (const notification of notifications)
  {
    const groupId = notification.id;
    if (groupId in versionGroups)
    {
      groups.push({
        id: groupId,
        variant: parseInt(versionGroups[groupId], 10)
      });
    }
  }

  return groups;
}

function assignGroups(groups, notifications)
{
  const selection = Math.random();
  let start = 0;

  for (const notification of notifications)
  {
    if (!("variants" in notification))
      continue;

    const groupIds = groups.map((group) => group.id);
    if (groupIds.includes(notification.id))
      continue;

    const group = {
      id: notification.id,
      variant: 0
    };
    groups.push(group);

    const {variants} = notification;
    for (let i = 0; i < variants.length; i++)
    {
      const sampleSize = variants[i].sample;
      const end = start + sampleSize;
      const selected = sampleSize > 0 && start <= selection && selection <= end;
      start = end;
      if (selected)
      {
        group.variant = i + 1;
        break;
      }
    }
  }
}

function getActiveVariant(notifications, groups)
{
  for (const group of groups)
  {
    const {id, variant} = group;
    if (variant === 0)
      continue;

    let notification = null;
    for (const x of notifications)
    {
      if (x.id === id)
      {
        notification = x;
        break;
      }
    }

    if (!notification)
      continue;

    // notification = copy.deepcopy(notification)
    // notification.update(notification['variants'][variant - 1]) // TODO: assign variant properties to notification itself
    for (const key of ["sample", "variants"])
    {
      delete notification[key];
    }

    return notification;
  }
}

function canBeShown(notification)
{
  return "title" in notification && "message" in notification;
}

function generateVersion(groups)
{
  const version = ""; // TODO: NYI
  const groupIds = groups.map((group) => `${group.id}/${group.variant}`);

  return [version, ...groupIds].join("-");
}

function getNotificationsToSend(notifications, groups)
{
  const activeVariant = getActiveVariant(notifications, groups);
  if (activeVariant)
    return (canBeShown(activeVariant)) ? [activeVariant] : [];

  const notificationsToSend = [];
  for (const notification of notifications)
  {
    if (!canBeShown(notification))
      continue;

    if ("variants" in notification)
    {
      // notification = copy.deepcopy(notification)
      delete notification.variants;
    }
    notificationsToSend.push(notification);
  }
  return notificationsToSend;
}

function getResponseData(notifications, groups)
{
  return {
    version: generateVersion(groups),
    notifications: getNotificationsToSend(notifications, groups)
  };
}

function onRequest(req, resp)
{
  if (req.url.indexOf("/notification.json") !== 0)
  {
    resp.writeHead(404);
    resp.end();
    return;
  }

  const urlInfo = url.parse(req.url);
  const params = qs.parse(urlInfo.query);
  const version = params.lastVersion || "";
  let notifications = loadNotifications(FILES);
  const groups = determineGroups(version, notifications);
  notifications = notifications
    .filter((notification) => notification.inactive !== false);
  assignGroups(groups, notifications);

  const respData = getResponseData(notifications, groups);
  const respBody = JSON.stringify(respData, null, 2);

  resp.writeHead(200, {
    "Content-Type": "application/json; charset=utf-8",
    "ABP-Notification-Version": respData.version
  });
  resp.write(respBody);
  resp.end();
}
exports.onRequest = onRequest;

if (require.main === module)
{
  http.createServer(onRequest)
    .listen(8080);
}
