/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function parseTargetSpec(value, name)
{
  const target = Object.create(null);

  const items = [
    [
      /^(extension|application|platform)(=)(.+)$/,
      {"=": (k, v) => [[k, v]]}
    ],
    [
      /^(extension|application|platform)Version(=|\>=|\<=)(.+)$/,
      {
        ">=": (k, v) => [[`${k}MinVersion`, v]],
        "<=": (k, v) => [[`${k}MaxVersion`, v]],
        "=": (k, v) => [[`${k}MinVersion`, v], [`${k}MaxVersion`, v]]
      }
    ],
    [
      /^(blockedTotal)(=|\>=|\<=)(\d+)$/,
      {
        ">=": (k, v) => [[`${k}Min`, parseInt(v, 10)]],
        "<=": (k, v) => [[`${k}Max`, parseInt(v, 10)]],
        "=": (k, v) => [[`${k}Min`, parseInt(v, 10)], [`${k}Max`, parseInt(v, 10)]]
      }
    ],
    [
      /^(locales)(=)([\w\-,]+)$/,
      {"=": (k, v) => [[k, v.split(",")]]}
    ],
  ];

  let spec;
  try
  {
    for (spec of value.split(" "))
    {
      for (const [re, ops] of items)
      {
        const match = re.exec(spec);
        if (match)
        {
          const [, key, op, value] = match;
          const updates = ops[op](key, value);
          for (const update of updates)
          {
            target[update[0]] = update[1];
          }
        }
      }
    }
  }
  catch (ex)
  {
    throw new Error(`Unknown target specifier '${spec}' in file '${name}'`);
  }

  return target;
}

function parseNotification(name, content)
{
  const notification = {
    id: name,
    severity: "information",
    message: {},
    title: {}
  };
  let current = notification;

  const lines = content.split("\n");
  for (const line of lines)
  {
    if (!/\S/.test(line))
      continue;

    if (/^\[.*\]$/.test(line))
    {
      current = {title: {}, message: {}};
      if (!("variants" in notification))
      {
        notification.variants = [];
      }
      notification.variants.push(current);
      continue;
    }

    if (!line.includes("="))
      throw new Error(`Could not process line '${line}' in file '${name}'`);

    const delimiterIdx = line.indexOf("=");
    const key = line.substring(0, delimiterIdx).trim();
    const value = line.substring(delimiterIdx + 1).trim();
    const isVariant = current !== notification;

    if (key === "inactive" && !isVariant)
    {
      current.inactive = !["", "0", "no", "false", "off"].includes(value.toLowerCase());
    }
    else if (key === "severity")
    {
      if (!["information", "critical", "normal", "relentless"].includes(value))
        throw new Error(`Unknown severity value '${value}' in file ${name}`);

      current.severity = value;
    }
    else if (key === "links")
    {
      current.links = value.split();
    }
    else if (/^title\./.test(key))
    {
      const locale = key.slice(6);
      current.title[locale] = value;
    }
    else if (/^message\./.test(key))
    {
      const locale = key.slice(8);
      current.message[locale] = value;
    }
    else if (key === "target")
    {
      const target = parseTargetSpec(value, name);
      if (!("targets" in notification))
      {
        current.targets = [];
      }
      current.targets.push(target);
    }
    else if (key === "sample" && isVariant)
    {
      current.sample = parseFloat(value);
    }
    else if (["start", "end"].includes(key))
    {
      const [year, month, day, hour, minute] = value.split(/[-T:]/g)
        .map((num) => parseInt(num, 10));
      current[key] = Date.UTC(year, month - 1, day, hour, minute);
    }
    else if (key === "interval")
    {
      const interval = parseInt(value, 10);
      if (Number.isNaN(interval))
        throw new Error(`Interval '${value}' is not a number in file '${name}'`);

      current[key] = interval;
    }
    else if (key === "urls")
    {
      current.urlFilters = value.split(" ")
        .map((v) => `${v.toUpperCase()}^$document`);
    }
    else
      throw new Error(`Unknown parameter '${key}' in file '${name}'`);
  }

  for (const textKey of ["title", "message"])
  {
    function hasDefaultLocale(variant)
    {
      return "en-US" in variant[textKey];
    }

    if (!hasDefaultLocale(notification))
    {
      const {variants = []} = notification;
      const variantsHaveDefaultLocale = variants.every(hasDefaultLocale);
      if (!variantsHaveDefaultLocale)
        throw new Error(`No ${textKey} for en-US (default language) in file '${name}'`);
    }
  }

  return notification;
}

function loadNotifications(files)
{
  const notifications = [];

  for (const [name, content] of files)
  {
    try
    {
      const notification = parseNotification(name, content);
      if (!("inactive" in notification))
      {
        const currentTime = Date.now();
        const startTime = notification.start || currentTime;
        const endTime = notification.end || currentTime;
        if (!(startTime <= currentTime && currentTime <= endTime))
        {
          notification.inactive = true;
        }

        delete notification.start;
        delete notification.end;
      }
      notifications.push(notification);
    }
    catch (ex)
    {
      console.error(ex);
    }
  }

  return notifications;
}
exports.loadNotifications = loadNotifications;
